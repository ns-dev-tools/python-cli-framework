from pipes.io_threads.abstract_io_thread import AbstractIOThread
import queue
import sys
import threading


class ConsoleWriteThread(AbstractIOThread):
    def __init__(self, fd=sys.stdout, io_queue_size=AbstractIOThread.DEFAULT_MAX_QUEUE_SIZE,
                 timeout=AbstractIOThread.DEFAULT_TIMEOUT):
        AbstractIOThread.__init__(self, fd, io_queue_size, timeout)

    def io_handler(self):
        try:
            result = self.io_queue.get_nowait()
            self.io_queue.task_done()
            self.fd.write(result)
            self.fd.flush()
        except queue.Empty:
            return None

    def check_if_io_requests_present(self):
        if not self.io_queue.empty():
            self.io_request_present.set()
            self.idle.clear()
        else:
            self.io_request_present.clear()
            self.idle.set()


class ConsoleReadThread(AbstractIOThread):
    def __init__(self, fd=sys.stdin, io_queue_size=AbstractIOThread.DEFAULT_MAX_QUEUE_SIZE,
                 timeout=AbstractIOThread.DEFAULT_TIMEOUT):
        AbstractIOThread.__init__(self, fd, io_queue_size, timeout)
        self.data_ready = threading.Event()
        self.data_ready.set()

    def io_handler(self):
        data = self.fd.readline()
        self.io_queue.put(data)
        self.data_ready.set()

    def check_if_io_requests_present(self):
        if not self.data_ready.is_set():
            self.io_request_present.set()
            self.idle.clear()
        else:
            self.io_request_present.clear()
            self.idle.set()