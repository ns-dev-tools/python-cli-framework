from pipes.io_threads.console_io_threads import ConsoleWriteThread, ConsoleReadThread

read_threads = dict()
write_threads = dict()


read_threads['console'] = ConsoleReadThread
write_threads['console'] = ConsoleWriteThread
