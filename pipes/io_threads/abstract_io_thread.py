import threading
import queue


class AbstractIOThread(threading.Thread):
    DEFAULT_MAX_QUEUE_SIZE = 4096
    DEFAULT_TIMEOUT = 5

    def __init__(self, fd, io_queue_size=DEFAULT_MAX_QUEUE_SIZE, timeout=DEFAULT_TIMEOUT):
        threading.Thread.__init__(self)
        self.daemon = True
        self.io_queue = queue.Queue(io_queue_size)
        self.timeout = timeout
        self.running = threading.Event()
        self.io_request_present = threading.Event()
        self.idle = threading.Event()
        self.fd = fd

    def io_handler(self):
        pass

    def check_if_io_requests_present(self):
        pass

    def run(self):
        self.running.set()
        self.idle.set()
        while self.running.is_set():
            self.io_request_present.wait(self.timeout)
            if self.io_request_present.is_set():
                self.io_handler()
            self.check_if_io_requests_present()
        self.io_request_present.clear()
