import uuid
import pipes
import pipes.io_threads


class IOPipe:
    def __init__(self, input_threads=None, output_threads=None):
        if input_threads is None:
            self.input_threads = []
        else:
            self.input_threads = input_threads
        if output_threads is None:
            self.output_threads = []
        else:
            self.output_threads = output_threads
        self.id = uuid.uuid4().hex
        pipes.pipes_dict[self.id] = self
        self.ref_count = 0

    def __eq__(self, other):
        if isinstance(other, IOPipe):
            return self.id == other.id
        return False

    def read(self):
        ##TO_DO some better handler
        result = None
        if len(self.input_threads) > 0:
            thread = None
            while thread is None:
                for input_thread in self.input_threads:
                    if input_thread.data_ready.is_set():
                        thread = input_thread
            thread.data_ready.clear()
            thread.io_request_present.set()
            result = thread.io_queue.get()
        return result

    def write(self, string):
        for thread in self.output_threads:
            thread.io_queue.put(string)
            thread.io_request_present.set()

    def flush(self):
        pass

    def readline(self):
        return self.read()

    def close(self):
        self.ref_count -= 1
        if self.ref_count == 0:
            for thread in self.input_threads:
                thread.running.clear()
            for thread in self.output_threads:
                thread.running.clear()
            #pipes.pipes_dict.pop(self.id)

    def start_new_read_thread(self, type_of_thread):
        if type_of_thread in pipes.io_threads.read_threads:
            self.input_threads.append(pipes.io_threads.read_threads[type_of_thread]())

    def start_new_write_thread(self, type_of_thread):
        if type_of_thread in pipes.io_threads.write_threads:
            self.output_threads.append(pipes.io_threads.write_threads[type_of_thread]())

    def open(self):
        if self.ref_count == 0:
            #pipes.pipes_dict[self.id] = self
            for thread in self.input_threads:
                thread.start()
            for thread in self.output_threads:
                thread.start()
        self.ref_count += 1
