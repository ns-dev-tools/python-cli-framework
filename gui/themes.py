class DefaultColorTheme:
    normal_text_color = 0x00FF00
    error_text_color = 0xFF0000
    success_text_color = 0x00FFFF
    critical_text_color = 0x800000
    warning_text_color = 0xFFFF00
