from gui.pretty_printing import PrettyPrinter
from gui import current_theme
from gui import loggers


class Loggable:
    def __init__(self, name, pipes):
        self.name = name
        if self.name in loggers:
            loggers[self.name].open_all_pipes()
        else:
            loggers[self.name] = PrettyPrinter(current_theme, pipes)

        self.logger = loggers[self.name]
