import gui.themes
import gui.pretty_printing
import pipes.io_pipe
import pipes.io_threads


MAIN_LOGGER_NAME = 'main_logger'

main_pipe = pipes.io_pipe.IOPipe()
main_pipe.start_new_read_thread('console')
main_pipe.start_new_write_thread('console')

current_theme = gui.themes.DefaultColorTheme

loggers = dict()
loggers[MAIN_LOGGER_NAME] = gui.pretty_printing.PrettyPrinter(current_theme, [main_pipe])

