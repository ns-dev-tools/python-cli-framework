import xtermcolor
from gui.generic_module_pipeline import GenericModulePipeline


class PrettyPrinter(GenericModulePipeline):
    def __init__(self, color_theme, pipes):
        GenericModulePipeline.__init__(self, pipes)

        self.color_theme = color_theme

        self.info_prefix = ""

        self.success_prefix = xtermcolor.colorize("[", rgb=color_theme.normal_text_color)\
            + xtermcolor.colorize("*", rgb=color_theme.success_text_color) \
            + xtermcolor.colorize("]", rgb=color_theme.normal_text_color) + "\t"

        self.error_prefix = xtermcolor.colorize("[", rgb=color_theme.normal_text_color)\
            + xtermcolor.colorize("!", rgb=color_theme.error_text_color) \
            + xtermcolor.colorize("]", rgb=color_theme.normal_text_color) + "\t"

        self.warning_prefix = xtermcolor.colorize("[", rgb=color_theme.normal_text_color)\
            + xtermcolor.colorize("!", rgb=color_theme.warning_text_color) \
            + xtermcolor.colorize("]", rgb=color_theme.normal_text_color) + "\t"

        self.critical_prefix = xtermcolor.colorize("[", rgb=color_theme.normal_text_color)\
            + xtermcolor.colorize("!", rgb=color_theme.critical_text_color) \
            + xtermcolor.colorize("]", rgb=color_theme.normal_text_color) + "\t"

    def info(self, message, id=None):
        if id is None:
            self.write_to_all(self.info_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))
        else:
            self.write_by_id(id, self.info_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))

    def success(self, message, id=None):
        if id is None:
            self.write_to_all(
                self.success_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))
        else:
            self.write_by_id(id,
                             self.success_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))

    def error(self, message, id=None):
        if id is None:
            self.write_to_all(self.error_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))
        else:
            self.write_by_id(id, self.error_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))

    def warning(self, message, id=None):
        if id is None:
            self.write_to_all(self.warning_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))
        else:
            self.write_by_id(id, self.warning_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))

    def critical(self, message, id=None):
        if id is None:
            self.write_to_all(self.critical_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))
        else:
            self.write_by_id(id, self.critical_prefix + xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))

    def print(self, message, id=None):
        if id is None:
            self.write_to_all(xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))
        else:
            self.write_by_id(id, xtermcolor.colorize(message, rgb=self.color_theme.normal_text_color))

