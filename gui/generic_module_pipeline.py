from pipes import pipes_dict


class GenericModulePipeline:
    def __init__(self, pipes=None):
        self.pipes = set()
        if pipes is None:
            return
        for pipe in pipes:
            self.pipes.add(pipe.id)
            pipes_dict[pipe.id].open()

    def add_pipe(self, pipe):
        if pipe.id not in self.pipes:
            pipes_dict[pipe.id].open()

    def delete_pipe(self, pipe):
        if pipe.id in self.pipes:
            pipes_dict[pipe.id].close()
            self.pipes.remove(pipe.id)

    def access_pipe(self, id):
        if id in self.pipes:
            return pipes_dict[id]
        else:
            return None

    def write_by_id(self, id, string):
        if id in self.pipes:
            pipes_dict[id].write(string + '\n')

    def write_to_all(self, string):
        for pipe_id in self.pipes:
            self.write_by_id(pipe_id, string)

    def read_by_id(self, id):
        if id in self.pipes:
            return pipes_dict[id].readln()

    def close_by_id(self, id):
        if id in self.pipes:
            pipes_dict[id].close()

    def close_all_pipes(self):
        for pipe_id in self.pipes:
            self.close_by_id(pipe_id)

    def open_by_id(self, id):
        if id in self.pipes:
            pipes_dict[id].open()

    def open_all_pipes(self):
        for pipe_id in self.pipes:
            self.open_by_id(pipe_id)

