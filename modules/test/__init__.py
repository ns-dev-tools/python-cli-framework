from pipes.io_pipe import IOPipe
from modules.test.test import MyModule


def init():
    module_pipe = IOPipe()
    module_pipe.start_new_read_thread('console')
    module_pipe.start_new_write_thread('console')
    return MyModule([module_pipe])
