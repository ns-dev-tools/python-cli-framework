from templates import module_template
from templates import module_command_thread_template
from templates import module_command_template
from pipes.io_threads.console_io_threads import *


DEFAULT_INPUT = sys.stdin
DEFAULT_OUTPUT = sys.stdout


class CommandFooThread(module_command_thread_template.CommandThreadTemplate):
    def __init__(self, name, pipes, *args, **kwargs):
        module_command_thread_template.CommandThreadTemplate.__init__(self, name, pipes, *args, **kwargs)
        self.args = args[0]

    def run(self):
        self.event_running.set()
        self.event_stop.clear()
        while not self.event_stop.is_set():
            for arg in self.args:
                self.logger.success(arg)
            self.event_stop.wait(10)
        self.event_running.clear()


class CommandFoo(module_command_template.CommandTemplate):
    def __init__(self, pipes):
        module_command_template.CommandTemplate.__init__(self, 'foo',
            'test function', CommandFooThread, pipes)


class MyModule(module_template.ModuleTemplate):
    def __init__(self, pipes):
        module_template.ModuleTemplate.__init__(self, 'MyModule', pipes)
        self.module_information = "This is a test module."
        self.commands["foo"] = CommandFoo(pipes)

    def do_foo(self, args):
        args = args.split()
        if len(args) > 1:
            self.run_command('foo', args[0], args[1:])
        elif len(args) == 1:
            self.run_command('foo', args[0], [])
        elif len(args) == 0:
            self.run_command('foo', None, [])

    def complete_foo(self, text, line, start_index, end_index):
        return self.inner_complete_command(text, line, start_index, end_index, 'foo')
