docopt==0.6.2
freeze==1.0.10
libpymux==0.1
pyte==0.8.0
six==1.11.0
wcwidth==0.1.7
xtermcolor==1.3
