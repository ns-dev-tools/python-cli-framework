import engine.engine as engine
import engine.engine_shell as engine_shell
import gui


def main():
    default_io_pipes = [gui.main_pipe, gui.main_pipe]
    shell = engine_shell.EngineShell(engine.Engine(default_io_pipes), default_io_pipes)
    shell.cmdloop()


if __name__ == "__main__":
    main()