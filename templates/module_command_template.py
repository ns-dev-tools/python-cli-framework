from templates.module_execution_result import ModuleExecutionResult
from gui.logging import Loggable
from pipes.io_pipe import IOPipe


class CommandTemplate(Loggable):
    def __init__(self, name, description, command_thread, pipes):
        Loggable.__init__(self, name, pipes)
        self.pipes = pipes
        self.name = name
        self.description = description
        self.thread_class = command_thread
        self.thread = None
        self.commands = ['info', 'run', 'stop']

    def info(self, *args):
        self.logger.print(self.description)
        return ModuleExecutionResult(returned_values=(True,))

    def run(self, *args):
        if self.is_running():
            return ModuleExecutionResult(False, error_message='Previous call is not finished yet')
        self.logger.success('Starting new job in another thread:')
        pipe = None
        for arg in args[0]:
            pos = arg.find('--pipe=')
            if pos != -1:
                if pipe is None:
                    pipe = IOPipe()
                pipe_type = (arg[pos+7:]).split('-')
                if pipe_type[0] == 'read':
                    pipe.start_new_read_thread(pipe_type[1])
                if pipe_type[0] == 'write':
                    pipe.start_new_write_thread(pipe_type[1])
                args[0].remove(arg)
        if pipe is not None:
            pipes = [pipe]
        else:
            pipes = self.pipes

        self.thread = self.thread_class(self.name + ": " + args[0][0], pipes, *args)
        self.thread.setDaemon(True)
        self.thread.start()
        return ModuleExecutionResult(returned_values=(True,))

    def is_running(self):
        return self.thread is not None and self.thread.event_running.is_set()

    def stop(self, *args):
        if self.thread is not None:
            self.thread.event_stop.set()
        return ModuleExecutionResult(returned_values=(True,))

    def add_output_thread(self, *args):
        pass

    def add_input_thread(self, *args):
        pass

    def get_commands(self, args=None):
        if args is not None and len(args) > 1:
            return []
        return self.commands

    def parse(self, *args):
        if len(*args)>0:
            if args[0][0] == 'run':
                return self.run(*args)
            elif args[0][0] == 'info':
                return self.info()
            elif args[0][0] == 'stop':
                return  self.stop()
            else:
                return ModuleExecutionResult(False, error_message='Unknown command')
        return ModuleExecutionResult(False, error_message='No command arguments supplied')
