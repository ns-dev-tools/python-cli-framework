import threading
from gui.logging import Loggable


class CommandThreadTemplate(threading.Thread, Loggable):
    def __init__(self, name, pipes, *args, **kwargs):
        threading.Thread.__init__(self, args=args, kwargs=kwargs)
        Loggable.__init__(self, name, pipes)
        self.event_stop = threading.Event()
        self.event_finished = threading.Event()
        self.event_running = threading.Event()

    def run(self, *args, **kwargs):
        self.event_running.set()
        self.event_stop.clear()
        while not self.event_stop.is_set():
            pass
        self.event_running.clear()
        self.logger.stop_all_pipes()

