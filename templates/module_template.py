import cmd
from gui.logging import Loggable
import time


class ModuleTemplate(cmd.Cmd, Loggable):
    def __init__(self, module_name, pipes):
        Loggable.__init__(self, module_name, pipes)
        if len(pipes) > 1:
            cmd.Cmd.__init__(self, stdin=pipes[0], stdout=pipes[1])
        elif len(pipes) == 1:
            cmd.Cmd.__init__(self, stdin=pipes[0], stdout=pipes[0])
        else:
            cmd.Cmd.__init__(self)
        self.module_information = None
        self.commands = {}
        self.module_name = module_name
        self.prompt = module_name + "#"

    def do_info(self, *args):
        if len(*args) is not 0:
            self.logger.warning("No arguments expected")
        self.logger.print(self.module_information)

    def do_quit(self, *args):
        self.logger.close_all_pipes()
        return -1

    def get_commands(self, args=None):
        if args is not None and len(args) > 1:
            return self.commands[args[1]].get_commands(args[1:])
        return [command for command in self.commands.keys()] + ['info', 'quit']

    def postcmd(self, stop, line):
        time.sleep(0.02)
        return super(ModuleTemplate, self).postcmd(stop, line)

    def inner_complete_command(self, text, line, start_index, end_index, command):
        args = line.split()

        if command not in self.commands:
            return []
        commands = self.commands[command].get_commands()
        if len(args) == 1 and command in self.commands:
            return commands
        if len(args) == 2 and text and command in self.commands:
            return [c for c in commands if command.startswith(text)]
        return []

    def run_command(self, command, action, *args):
        if command in self.commands:
            if action in self.commands[command].get_commands():
                return getattr(self.commands[command], action)(*args)
            else:
                self.logger.warning("Action %s not appliable to command %s" % (action, command))
        else:
            self.logger.warning("Command %s is unavailable" % command)
