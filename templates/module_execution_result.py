class ModuleExecutionResult:
    def __init__(self, is_success=True, returned_values=None, error_message=None):
        self.is_success = is_success
        self.error_message = error_message
        self.returned_values = returned_values
