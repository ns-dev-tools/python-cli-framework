from cmd import Cmd
from gui.logging import Loggable
import time


class EngineShell(Cmd, Loggable):
    name = 'engine-shell'

    def __init__(self, engine, pipes):
        Loggable.__init__(self, self.name, pipes)
        if len(pipes) > 1:
            Cmd.__init__(self, stdin=pipes[0], stdout=pipes[1])
        elif len(pipes) == 1:
            Cmd.__init__(self, stdin=pipes[0], stdout=pipes[0])
        else:
            Cmd.__init__(self)
        self.engine = engine
        self.prompt = "#"

    def do_info(self, args):
        """
        Print information about the engine.
        arguments: no arguments expected
        """
        self.logger.info(self.engine.get_version())

    def do_quit(self, args):
        """
        Quit the program.
        arguments: no arguments expected
        """

        self.logger.close_all_pipes()
        return -1

    def postcmd(self, stop, line):
        time.sleep(0.02)
        return super(EngineShell, self).postcmd(stop, line)

    def complete_module(self, text, line, start_index, end_index):
        args = line.split()

        get_all_choices = start_index == len(line)

        if len(args) == 1:
            return [module for module in self.engine.get_loaded_modules()]
        if len(args) == 2 and text:
            return [module for module in self.engine.get_loaded_modules() if module.startswith(text)]

        if args[1] not in self.engine.get_loaded_modules():
            return []

        if get_all_choices:
            return self.engine.get_commands_for_module(args[1:])
        else:
            return [command for command in self.engine.get_commands_for_module(args[1:-1]) if command.startswith(text)]

    def do_module(self, args):
        """
        Call command from the selected module with given arguments.
        If called without any arguments prints out list of currently loaded modules
        arguments:
        - module name
        - command name
        - command arguments
        """

        args = args.split()
        if len(args) > 2:
            self.engine.call(args[0], args[1], args[2:])
        elif len(args) == 2:
            self.engine.call(args[0], args[1], [])
        elif len(args) == 1:
            self.engine.call(args[0], None, [])
        else:
            self.logger.info("Currently loaded modules:")
            for module in self.engine.get_loaded_modules():
                self.logger.info(module)

# def default(self, args):

