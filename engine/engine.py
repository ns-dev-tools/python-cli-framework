import importlib.util
from os import listdir
from os.path import isdir, join
from gui.logging import Loggable
from templates.module_template import ModuleTemplate


class Engine(Loggable):
    name = 'engine'
    default_module_path = ("./modules/", "modules")
    version_string = "CLI Framework v0.3 by Niko"

    def __init__(self, pipes, module_path=None):
        Loggable.__init__(self, self.name, pipes)
        if module_path is None:
            module_path = self.default_module_path
        self.logger.info("Loading module list from path [" + module_path[0] + "]")

        self.modules = {}
        self.load_modules_from_directory(module_path)

        if len(self.modules) > 0:
            self.logger.success('Currently loaded modules:')
        for module in self.modules.keys():
            self.logger.info('\t' + module)
        if len(self.modules) == 0:
            self.logger.warning("No modules loaded")

    def load_module(self, module_name, module_path):
        try:
            module = importlib.import_module(module_path + "." + module_name)

            if hasattr(module, 'init'):
                module = module.init()
                if isinstance(module, ModuleTemplate):
                    self.modules[module_name] = module
                else:
                    self.logger.warning("init() from module [%s] doesn't return an instance of class derived"
                                        " from ModuleTemplate")
            else:
                self.logger.warning("Module [%s] is not a subclass of ModuleTemplate" % module_name)
        except ModuleNotFoundError:
            self.logger.warning("Module [%s] not found" % module_path + "." + module_name)

    def load_modules_from_directory(self, path):
        module_names = [f for f in listdir(path[0]) if (
                not f.startswith("__") and
                isdir(join(path[0], f))
        )]
        modules_base_package = path[1]
        if len(module_names) == 0:
            self.logger.warning("No modules found in %s" % path[0])

        for module_name in module_names:
            self.load_module(module_name, modules_base_package)

    def get_loaded_modules(self):
        return self.modules.keys()

    def get_commands_for_module(self, args):
        if args[0] not in self.modules:
            self.logger.warning("Requested module [%s] is not loaded" % args[0])
            return []
        return self.modules[args[0]].get_commands(args)

    def get_loaded_module_info(self, module_name):
        if module_name not in self.modules:
            self.logger.warning("Requested module [%s] is not loaded" % module_name)
            return ""
        else:
            return self.modules[module_name].get_info()

    def get_version(self):
        return self.version_string

    def call(self, module_name, command, *args):
        if module_name in self.modules:
            if command is None:
                self.modules[module_name].cmdloop()
            else:
                line = command + ' ' + '' .join(str(x) + ' ' for x in args[0])
                self.modules[module_name].onecmd(line)
        else:
            self.logger.error("Module " + module_name + " is not loaded")
